#!/home/sneubert/miniconda2/bin/python
import cgi
import cgitb
cgitb.enable()


import couchdb, os
from utils import *

def listhtm() :

    couch = couchdb.Server()
    db=couch["wgdb"]

    form = cgi.FieldStorage()
    analysis_id = form.getfirst('id', '0')

    data=db[analysis_id];

    # check if there is something to upload
    upload_dir = "/tmp"
    if form.has_key("attachfile"):
        fileitem = form["attachfile"]
        if fileitem.file:
            db.put_attachment(data, fileitem.file, fileitem.filename, content_type=fileitem.type)
            data=db[analysis_id]; # reload updated entry

    # lookup linked documents (can this be done in a couchdb view??)
    persons=data["person"];
    proponents=reduce((lambda x,y : x+", "+y), [db[p]["name"] for k,p in persons.iteritems()]);
    sub=db[data["microgroup"]]["name"];


    presentations=resolveDBlinks(db,data,"report","link")
    collabpresi=resolveDBlinks(db,data,"collabreport","link")
    ananotes=resolveDBlinks(db,data,"ananote","name")

    # put in linked info
    data["proponents"]=proponents;
    data["subgroup"]=sub;
    data["presentations"]=presentations;
    data["ananotes"]=ananotes;

    doc = '''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1-transitional.dtd">
    <!doctype html>
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/JavaScript">
    jQuery(document).ready(function(){
        $('#accordion h3').addClass("ui-accordion-header ui-accordion-header-collapsed ui-corner-all ui-state-default ui-accordion-icons");
        $('#accordion h3').next().addClass("ui-accordion-content ui-corner-bottom ui-helper-reset ui-widget-content ui-accordion-content-active");
        $('#accordion h3').click(function() {
            $(this).next().toggle();
            $(this).toggleClass("ui-accordion-header-active");
            $(this).toggleClass("ui-state-active");
            $(this).toggleClass("ui-corner-all");
            $(this).toggleClass("ui-corner-top");
            $(this).toggleClass("ui-accordion-header-collapsed");
            return false;
        }).next().hide(); '''
    # open the documentation tab after an upload
    if form.has_key("attachfile"):
        doc+='''
             $('#accordion #attachments').trigger("click");
             '''
    doc+='''
        $('#accordion #summary').trigger("click");
        $('input:file').change(
            function(){
                if ($(this).val()) {
                    $('input:submit').attr('disabled',false);
                    // or, as has been pointed out elsewhere:
                    // $('input:upload').removeAttr('disabled');
                }
            }
        );
    });
    </script>




    <title>Analysis</title>
    </head>
    '''
# add data
    doc+='''
    <body>
    <h2><a href="analysis.py?id={_id}">{name}</a></h2>
<div id="accordion" class="ui-accordion ui-widget ui-helper-reset">
  <h3 id="summary" role="tab">Summary</h3>
  <div>'''.format(**data)
    doc+=dict2html({"status":"Status","subgroup":"SubWG","proponents":"Proponents","paper":"Paper","arxiv":"arXiv"},
                    data,"tab:summary","display dataTable no-footer",
                    {"paper":'<a target="_blank" href="http://lhcbproject.web.cern.ch/lhcbproject/Publications/LHCbProjectPublic/{val}.html">{val}</a>',
                     "arxiv":'<a target="_blank" href="http://arxiv.org/abs/{val}">{val}</a>'})
    doc+='''
    <br>
   <div id="editicon"><img width="25" src="../res/edit.svg"></div>
   </div>
  <h3>Review</h3>
  <div>'''
    doc+=dict2html({"egroup":"Egroups","rc":"Reviewers","ebpage":"EB page","signdate":"Sign-off"},
                    data,"tab:review","display dataTable no-footer",
                    {"egroup":'<a target="_blank" href="https://groups.cern.ch/group/{val}">{val}</a>',
                     "ebpage":link()})
    doc+='''
    <br>
    <div id="editicon"><img width="25" src="../res/edit.svg"></div>
  </div>
  <h3 id="documentation">Documentation</h3>
  <div>
    <p>'''
    doc+=dict2html({"twiki":"Twiki","presentations":"Presentations","ananotes":"ANA notes","gitlab":"gitlab"},
                    data,"tab:docu","display dataTable no-footer",
                    {"twiki":link(),"presentations":link(),"ananotes":link(),"gitlab":link()})

    doc+='''
    <br>
    <div id="editicon"><img width="25" src="../res/edit.svg"></div>
    </div>
    <h3 id="attachments">Attachments</h3>
    <div>
     <form action="analysis.py" method="POST" enctype="multipart/form-data">
    Add attachment: <input id="attachfile" name="attachfile" type="file">
    <input name="id" type="hidden" value={_id}>
    <input name="upload" disabled=true value="Upload" type="submit">
    </form>'''.format(**data)

    # check if there are attachments
    try:
        att=data["_attachments"]
        from PIL import Image
        size = 128,128
        doc+="<p>"
        for a in att :
            doc += str(a)+"    "
            im=Image.open(db.get_attachment(analysis_id,a))
            outfile = str(a) + ".thumbnail" + ".jpg"
            im.thumbnail(size)
            im.save(outfile, "JPEG")
            doc+='<img src="../{}">'.format(outfile)
        doc+="</p>"
    except : pass

    doc+='''
  </div>
  <h3>Meta Information</h3>
  <div>'''
    doc+=dict2html({"stripline":"Stripping lines","sample":"Sample"},
                    data,"tab:meta","display dataTable no-footer")

    doc+='''
    <br>
    <div id="editicon"><img width="25" src="../res/edit.svg"></div>
    </div>
  </div>

 </div>
</body></html>
 '''




    return doc

print listhtm()
