# tools for html formatting

def link(myurl="{val}") :
    return '<a target="_blank" href="{url}">{url}</a>'.format(url=myurl)

def dict2html(keys, data, id="dict", htmclass="", paddings={}) :
    '''
    print the contents of a dictionary in a two-column tables
    keys is a dictionary containing the keys to use and the strings to show in the table
    paddings can be used to add formatting (such as a href) around a value
    '''
    s='<table id="{}" class="{}">'.format(id,htmclass)
    for key,name in keys.iteritems() :
        padding=paddings.get(key,"{val}") # by default we will just insert the value later
        if key in data.keys() :
           ob=data[key]
           # check if we have a dict
           if isinstance(ob,dict) :
            ob="<table>"
            for i,item in data[key].iteritems() :
                  ob+='<tr><td id="tabledata">{}</td></tr>'.format(padding.format(val=item))
            ob+="</table>"
           else :
               # dirty hack:
               if key=='paper' : ob=ob.replace("LHCB","LHCb")
               ob=padding.format(val=ob)
           # add a row
           s+='<tr><td>{}</td><td id="tabledata">{}</td></tr>'.format(name,ob)
    s+="</table>"
    return s


def resolveDBlinks(db,data,key,field="name") :
    '''
    data is a document containing a list of couchdb ids at key
    resolve those ids for the objects and retrieve the list of fields
    return a dictionary containg the resolved data
    '''
    if not key in data.keys(): return {}
    objs=data[key]
    newdict={}
    try: # key might point to a list of references
        for i,item in objs.iteritems():
            newdict[i]=db[item][field]
    except : # single object
        newdict["0"]=objs
    return newdict
