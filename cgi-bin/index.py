#!/home/sneubert/miniconda2/bin/python
import cgi
import cgitb
cgitb.enable()



import couchdb

def listhtm() :

    couch = couchdb.Server()
    db=couch["wgdb"]



    doc = '''
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1-transitional.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Analyses</title>
    </head>


    <body>
    <table id="table_id" class="display">
    <thead>
    <tr>
            <th>Name</th>
            <th>Status</th>
            <th>Subgroup</th>
        </tr>
    </thead>
    <tbody>

    '''

# proper DB access using a view
    map_fun = '''function(doc) {
             {
                 emit(doc.category, {"name" : doc.name,
                                     "id"   : doc._id,
                                     "subgroup" : doc.microgroup,
                                     "status" : doc.status});
             }
            }'''

    results = db.query(map_fun)
    analyses = list(results["analysis"])
    for r in analyses :
        doc+='<tr><td><a href="analysis.py?id='+r.value["id"]+'">'+r.value["name"]+"</a></td>"
        try:
          doc+="<td>"+r.value["status"] + "</td>"
        except :
          doc+="<td>not assigned</td>"
        try :
          subgroup=db[r.value["subgroup"]]["name"]
        except :
          subgroup="not assigned"
        doc+="<td>"+subgroup+"</td></tr>"
# dump db access
    # count=0;
    # for id in db :
    #     entry=db[id]
    #     if entry["category"]=="analysis" :
    #         doc+="<tr><td>"+entry["name"]+"</td>"
    #         try :
    #             subgroup=db[entry["microgroup"]]["name"]
    #         except :
    #             subgroup="none"
    #         doc+="<td>"+subgroup+"</td></tr>"
    #         count+=1
    #         if count>20 : break
    #
    #
    #
    doc+='''
    </tbody></table>
    <script type="text/JavaScript" src="//code.jquery.com/jquery-1.12.4.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
    <script type="text/JavaScript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>
    <script type="text/JavaScript" charset="utf8" src="//cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">'''

    doc+='''
    <script type="text/JavaScript" charset="utf8">
    $(document).ready( function () {
        $('#table_id').DataTable();
    } );
    </script>'''

    doc+='''</body></html>
    '''
    return doc

print listhtm()
